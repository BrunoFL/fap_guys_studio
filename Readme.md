# Nom du jeu

Jeu fait à DPS Université de Franche-comté.

## Lancer le jeu

Le jeu est disponible à cette adresse : http://rnday.fr/fap/index.html


- Lancer dans un navigateur (chrome) index.html
- Clic droit -> inspecter
- Cliquer sur l'icone representant un telephone/tablette
- Simuler un telephone en mode portrait (1080x1920)

## Le jeu

Le but du jeu est de se masturber le plus longtemps sans jouir. La jauge d'endurance possede un seuil à ne pas depasser. Si on depasse le seuil la partie est finie et on finit. Le niveau d'excitation est calculé en fonction de :

- L'image sur l'ecran
- La fréquence des "pan" du joueur
- Les effets des objets

Plus on est proche du seuil, plus la frequence est haute, plus on gagne de points, les objets ont une incidance sur le multiplicateur.

Avant de jouer on peut personnaliser son objet du désir gràce aux points gagnés précédement.

## Idées :

Jeu de type clicker

- Un écran diffuse des images qui influe sur l'excitation
- Panic button, rend l'écran tout noir

## Gameplay :

- On peut jouer une fille ou un garcon
- L'idée est de se masturber
- Jauge d'endurance/excitation, plus on fait de fap, plus on augmente dans la jauge. Plus on est haut, plus on gagne de points à chaque fap. Attention, si on dépasse la jauge on revient à 0 et on "finit" ;). Cela induit une période réfractaire où on ne peut plus gagner de points
- Reconaissance de mouvement pour gagner plus de points

## Magasin :

Liste des achats possible :

- Connexion internet haut débit, rend les images de meilleurs qualités, monte plus facilement
- Mouchoirs, diminue le temps de la période refractaire
- Legumes, augmente l'excitation

## Consommables :

Liste des consommables utilisables

- Viagra, la jauge est plus longue
- Lubrifiant, la montée dans la jauge est plus facile

## Customisations :

- Couleurs de peau
- Formes
- Ajouts : piercings, tatouages, veines, ...

### Themes à implanter :

- Métal
- Lipossucion
- Julien Bernard
- Légumes et produits de nos terroirs
- Francois Fillon contre Balkany

### Release 1 :

Chose qui doit fonctionner pour la release 1 :

- On se branle, la jauge monte
- La jauge descend petit à petit
- Les images défilent
- Un début une fin

### Release 2

Ajout de fonctionnalités :

- Mode fille
- Personnalisation !
