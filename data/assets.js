var dureeSoundFap = 100;

var manifest = [
    {"id" : "main", "src" : "assets/img/main.png"}, {
        "id" : "createjs-badge-dark",
        "src" : "assets/img/createjs/createjs-badge-dark.png"
    },
    {
      "id" : "createjs-badge-light",
      "src" : "assets/img/createjs/createjs-badge-light.png"
    },

    {"id" : "dick_base", "src" : "assets/img/bite/foreground.png"},
    {"id" : "fingers", "src" : "assets/img/fingers.png"},
    {"id" : "thumb", "src" : "assets/img/thumb.png"},
    {"id" : "ecran0", "src" : "assets/img/ecran/p.jpg"},
    {"id" : "ecran1", "src" : "assets/img/ecran/p2.jpg"},
    {"id" : "ecran2", "src" : "assets/img/ecran/p3.jpg"},
    {"id" : "ecran3", "src" : "assets/img/ecran/p4.png"},
    {"id" : "ecran4", "src" : "assets/img/ecran/p5.png"},
    {"id" : "jb", "src" : "assets/img/jb.png"},
    {"id" : "jambes_sfw", "src" : "assets/img/detourmeufbonne_sfw.png"}
];

var manifestSons = [
    {
      "src" : "assets/sound/fap.mp3",
      data : {
          audioSprite : [
              {id : "fap0", startTime : 0, duration : dureeSoundFap}, {
                  id : "fap1",
                  startTime : dureeSoundFap,
                  duration : 2 * dureeSoundFap
              },
              {
                id : "fap2",
                startTime : 2 * dureeSoundFap,
                duration : 3 * dureeSoundFap
              },
              {
                id : "fap3",
                startTime : 3 * dureeSoundFap,
                duration : 4 * dureeSoundFap
              },
              {
                id : "fap4",
                startTime : 4 * dureeSoundFap,
                duration : 5 * dureeSoundFap
              },
              {
                id : "fap5",
                startTime : 5 * dureeSoundFap,
                duration : 6 * dureeSoundFap
              },
              {
                id : "fap6",
                startTime : 6 * dureeSoundFap,
                duration : 7 * dureeSoundFap
              },
              {
                id : "fap7",
                startTime : 7 * dureeSoundFap,
                duration : 8 * dureeSoundFap
              },
              {
                id : "fap8",
                startTime : 8 * dureeSoundFap,
                duration : 9 * dureeSoundFap
              }
          ]
      }
    },
    {"id" : "groove", "src" : "assets/sound/groove_low.mp3"}
];

var nbFap = 8;
var nbImgsEcran = 5;
