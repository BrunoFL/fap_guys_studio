// Intialisation canvas
var cvs = document.getElementById('cvs');
var width = window.innerWidth || document.documentElement.clientWidth ||
            document.body.clientWidth;
var height = window.innerHeight || document.documentElement.clientHeight ||
             document.body.clientHeight;
cvs.width = cvs.getContext('2d').width = width;
cvs.height = cvs.getContext('2d').height = height;
var game = new createjs.Stage('cvs');
var excitationLevel = 0;  // Réel de 0 à 1
var fps = 60;
var cptPointHTML = document.getElementById("cptPoints");
var cptPoints = 0;
var pileScore = [];
var pouce, main, bite;
var jb, schnek;
var isMale = true;

// initialisation
var men = document.getElementById('menu');
var bar = new ProgressBar.Line('#progressBar', {
    easing : 'easeInOut',                          // Fonction d'animation
    duration : 1000 / fps,                         // Temps de l'animation (ms)
    color : '#a5ff82',                             // Couleur de la barre
    trailColor : '#eee',                           // Couleur du trait
    trailWidth : 100,                              // Epaisseur du trait
    svgStyle : {width : '100%', height : '100%'},  // Forme du trait (barre)
    from : {color : '#a5ff82'},                    // Couleur début
    to : {color : '#ff2b2b'},                      // Couleur fin
    step : (state, bar) => {
        bar.path.setAttribute('stroke', state.color);
    }  // Fonction "qui anime la couleur"
});
// bar.animate(0.8);  // Number from 0.0 to 1.0
document.getElementById("btnFemale").addEventListener("click", switch2Female);
document.getElementById("btnFemale").addEventListener("touch", switch2Female);
document.getElementById("btnMale").addEventListener("click", switch2Male);
document.getElementById("btnMale").addEventListener("touch", switch2Male);

createjs.Sound.alternateExtensions = [ "mp3" ];
var assets = new createjs.LoadQueue(false);
assets.on("complete", function(e) {
    createjs.Sound.registerPlugins([ createjs.HTMLAudioPlugin ]);
    var assets2 = new createjs.LoadQueue(false);
    assets2.installPlugin(createjs.Sound);
    assets2.on("complete", handleComplete, this);  // Evenement chargement fini
    assets2.loadManifest(manifestSons);            // Elements à charger
}, this);                                          // Evenement chargement fini
assets.loadManifest(manifest);                     // Elements à charger

var main;

// Elements chargés
function handleComplete() {
    // Affichage du studio et nom du jeu

    setTimeout(function() {
        document.getElementById('loading').style.backgroundImage =
            "url('assets/img/learn_to_fap.png')";
        setTimeout(function() {
            document.getElementById('loading').style.display = 'none';
        }, 3000);
    }, 2000);

    // Mise a jour 60fps
    createjs.Ticker.setFPS(fps);
    createjs.Ticker.addEventListener("tick", game);
    createjs.Ticker.on("tick", gameTick);

    carouselInit();
    initMale();

    createjs.Sound.play("groove");

    beginGame();

    document.getElementById('retry').addEventListener("touch", beginGame,
                                                      {passive : false});
    document.getElementById('retry').addEventListener("click", beginGame,
                                                      {passive : false});
};

function gameTick(event) {
    if (excitationLevel > 0.005) {
        excitationLevel *= 0.98;
    } else {
        excitationLevel = 0;
    }
    bar.animate(excitationLevel);

    var bound = main.getBounds();

    if (isMale) {
        main.y = pouce.y = (height * (0.5)) + (lastY * (width * 0.5) / height);
    } else {
        jb.y = (height * (0.5)) + (lastY * (width * 0.5) / height);
    }

    game.setChildIndex(imgEcran, game.getNumChildren());
    game.update();
};

function randInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};

document.addEventListener("endGame", endGame);
function endGame() {
    ejac();
    setTimeout(function() {
        document.getElementById('retry').style.opacity = '1';
        document.getElementById('final_BG').style.opacity = '1';

        document.getElementById('end').style.opacity = '1';
        document.getElementById('img_splash').style.opacity = '1';
        document.getElementById('final_BG').style.opacity = '0';
        document.getElementById('retry').style.opacity = '0';
        document.removeEventListener("mousemove", touchHandler);
        document.removeEventListener("touchmove", touchHandler);

        setTimeout(function() {
            document.getElementById('retry').style.opacity = '1';
            document.getElementById('final_BG').style.opacity = '1';
            document.getElementById('score_final').innerHTML = parseInt(score);
        }, 1500);
    }, 1500);
};

function beginGame() {
    // closeNav();
    document.getElementById('end').style.opacity = '0';
    score = 0;
    // document.ontouchmove = function(event){
    //     console.log("PREVENT");
    //     // event.preventDefault();
    // }
    document.addEventListener("mousemove", touchHandler, {passive : false});
    document.addEventListener("touchmove", touchHandler, {passive : false});
};

function ejac() {
    var nb = Math.floor(Math.random() * 100 + 50);
    for (var i = 0; i < nb; i++) {
        var circles = [];
        var circle = new createjs.Shape();
        circles.push(circle);
        var d = Math.floor(Math.random() * 6);

        circle.x = width / 2 - 25;
        if (isMale) {
            circle.graphics
                .beginFill("rgb(2" + d + "5,2" + d + "5,2" + d + "5)")
                .drawEllipse(20, 20, 10, 80);
            circle.y = height / 2 - height * 0.1;
        } else {
            circle.graphics.beginFill("rgb(2" + d + "5, 255, 255)")
                .drawEllipse(20, 20, 10, 80);
            circle.y = height * 0.7;
        }
        game.addChild(circle);
        var time = Math.floor(Math.random() * 1000 + 600);
        var x = Math.random() * 200 - 100;
        createjs.Tween.get(circle)
            .to({x : width / 2 + x, y : height / 4}, time)
            .to({x : width / 2, y : -100}, time + 200)
            .call(function() { game.removeChild(this); });
    }
};

function switch2Female() {
    document.getElementById('html').style.backgroundImage =
        "url('assets/img/bg.png')";
    var icon_male = document.getElementById("btnMale");
    var icon_female = document.getElementById("btnFemale");
    icon_male.classList.remove("non-visible");
    icon_female.classList.add("non-visible");
    isMale = false;
    initFemale();
};

function switch2Male() {
    document.getElementById('html').style.backgroundImage =
        "url('assets/img/bgM.jpg')";
    var icon_male = document.getElementById("btnMale");
    var icon_female = document.getElementById("btnFemale");
    icon_male.classList.add("non-visible");
    icon_female.classList.remove("non-visible");
    isMale = true;
    initMale();
};

function initMale() {
    if (schnek) game.removeChild(schnek);
    if (jb) game.removeChild(jb);

    main = new createjs.Bitmap(assets.getResult("fingers"));
    var bound = main.getBounds();
    main.scaleX = width / bound.width;
    main.scaleY = height / bound.height * 0.6;
    main.x = width * 0.2;
    game.addChild(main);

    // Exemple ajout image
    bite = new createjs.Bitmap(assets.getResult("dick_base"));
    bound = bite.getBounds();
    bite.scaleX = width / bound.width * 0.8;
    bite.scaleY = height / bound.height * 0.9;
    bite.y = (height - bound.height * bite.scaleY);
    bite.x = (width - bound.width * bite.scaleX) / 2;
    game.addChild(bite);

    pouce = new createjs.Bitmap(assets.getResult("thumb"));
    bound = pouce.getBounds();
    pouce.scaleX = main.scaleX;
    pouce.scaleY = main.scaleY;
    pouce.x = width * 0.2;
    game.addChild(pouce);
};

function initFemale() {
    if (bite) game.removeChild(bite);
    if (pouce) game.removeChild(pouce);
    if (main) game.removeChild(main);

    jb = new createjs.Bitmap(assets.getResult("jb"));
    var bound = jb.getBounds();
    jb.scaleX = width / bound.width * 0.4;
    jb.scaleY = height / bound.height * 0.2;
    jb.x = (width - bound.width * jb.scaleX) / 2;
    game.addChild(jb);

    schnek = new createjs.Bitmap(assets.getResult("jambes_sfw"));
    bound = schnek.getBounds();
    schnek.scaleX = width / bound.width * 1.4;
    schnek.scaleY = height / bound.height * 0.65;
    schnek.x = (width - bound.width * schnek.scaleX) / 2;
    schnek.y = (height - bound.height * schnek.scaleY);

    game.addChild(schnek);
}
