var cptEcran = 0, imgEcran, containerEcran;

function carousel() {
    if (imgEcran) {
        containerEcran.removeChild(imgEcran);
        cptEcran = (cptEcran + 1) % nbImgsEcran;
    }

    imgEcran = new createjs.Bitmap(assets.getResult("ecran" + cptEcran));
    var x = width * 0.33;
    var y = height * 0.205;
    var bound = imgEcran.getBounds();
    var sX = width / bound.width * 0.48;
    var sY = height / bound.height * 0.17;
    imgEcran.setTransform(x, y, sX, sY, 2.8);
    containerEcran.addChild(imgEcran);

    var time = 2000 + Math.abs(Math.random() * 2000 - 1000);
    setTimeout(carousel, time);
};

function carouselInit() {
    containerEcran = new createjs.Container();
    game.addChild(containerEcran);
    carousel();
};