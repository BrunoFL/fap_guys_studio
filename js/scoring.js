document.addEventListener("panUpDown", handlePanUpDown);
var score = 0;
function handlePanUpDown(event) {
    length = event.detail.duration;
    // console.log(length)
    excitationLevel += 0.002 * length;
    if (excitationLevel > 1) {
        excitationLevel = 1;
        var evnt = new Event("endGame");
        document.dispatchEvent(evnt);
    }

    pts = length * excitationLevel;
    score += pts / 10;
    createjs.Sound.play("fap" + randInt(0, nbFap - 1));

    // Affiche les points
    var divPts = document.createElement('div');
    divPts.innerHTML = Math.floor(score);
    cptPointHTML.appendChild(divPts);
    pileScore.push(divPts);
    setTimeout(function() {
        var div = document.getElementById("cptPoints");
        div.removeChild(pileScore.shift());
    }, 900);
};
