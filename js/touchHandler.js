var lastY = 0;
var lastX = 0;
var state = null;
var changedSens = 0;
var init = false;
var firstY = 0;

function touchHandler(event) {
    event.preventDefault();
    // console.log(event);
    var noClick =
        event.changedTouches != null && event.changedTouches.length > 0;
    // console.log(noClick);
    if (!init) {
        if (noClick) {
            // console.log("init");
            state = 1;
            lastY = event.changedTouches[0].clientY;
            init = true;
            firstY = lastY;
        }
        // lastX = event.changedTouches[0].clientX;
        }
    if (noClick && event.changedTouches[0].clientY > lastY) {
        lastY = event.changedTouches[0].clientY;
        // lastX = event.changedTouches[0].clientX;

        switch (state) {
            case 1:
                state = 3;
                break;
            case 2:
                state = 3;
                changeSens(lastY);
                break;
            case 3:
                state = 3;
                break;
            default:
        }
        // state = 3;
        }
    if (noClick && event.changedTouches[0].clientY < lastY) {
        lastY = event.changedTouches[0].clientY;
        // lastX = event.changedTouches[0].screenX;

        switch (state) {
            case 1:
                state = 2;
                break;
            case 2:
                state = 2;
                break;
            case 3:
                state = 2;
                changeSens(lastY);
                break;
            default:
        }
        // state = 2;
    }
    }

function changeSens(lastY2) {
    changedSens++;
    if (changedSens % 2 == 0) {
        var event = new CustomEvent(
            "panUpDown", {"detail" : {"duration" : Math.abs(firstY - lastY2)}});
        document.dispatchEvent(event);
        // changedSens = 0;
        firstY = lastY;
    }
}
